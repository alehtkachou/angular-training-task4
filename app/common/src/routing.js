app.config(function ($stateProvider, $urlRouterProvider) {

    "use strict";

    $urlRouterProvider.otherwise("/main/info");

    $stateProvider
        .state('main', {
            url: "/main",
            views: {
                "mainview": {
                    templateUrl: 'app/pages/src/home/src/tpl/home.tpl.html',
                    controller: function ($state) {
                        $state.go("main.info");
                    }
                }
            },
            resolve: {
                auth: ["$q", "loginService", function ($q, loginService) {
                    var userInfo = loginService.getUserInfo();
                    if (userInfo) {
                        return $q.when(userInfo);
                    } else {
                        return $q.reject({ authenticated: false });
                    }

                }]
            }
        })
        .state('login', {
            url: "/login",
            views: {
                "mainview": {
                    templateUrl: 'app/pages/src/login/src/tpl/login.tpl.html',
                    controller: "LoginController"
                }
            }
        })
        .state('main.info', {
            url: "/info",
            views: {
                "childview": {
                    templateUrl: 'app/pages/src/info/src/tpl/user-info.tpl.html',
                    controller: "InfoController"
                }
            }
        })
        .state('main.edit', {
            url: "/edit",
            views: {
                "childview": {
                    templateUrl: 'app/pages/src/edit/src/tpl/user-edit.tpl.html',
                    controller: "EditController"
                }
            }
        })
        .state('main.dashboard', {
            url: "/dashboard",
            views: {
                "childview": {
                    templateUrl: 'app/pages/src/dashboard/src/tpl/dashboard.tpl.html',
                    controller: "ChartController"
                }
            }
        })
});