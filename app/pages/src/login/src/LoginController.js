app.controller("LoginController", ["$scope", "$window", "$state", "loginService", function ($scope, $window, $state, loginService) {

    "use strict";

    $scope.userInfo = null;

    $scope.login = function () {
        loginService.login($scope.userName, $scope.password)
            .then(function (result) {
                $scope.userInfo = result;
                $state.go("main.info");
            }, function (error) {
                $window.alert("Invalid credentials");
                console.log(error);
            });
    };

    $scope.cancel = function () {
        $scope.userName = "";
        $scope.password = "";
    };
}]);