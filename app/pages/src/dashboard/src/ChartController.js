app.controller("ChartController", ["$scope", "loadService", function ($scope, loadService) {

    var names;

    loadService.getDataChart().then(function (result) {
        names = result.data.names;
        $scope.names = names;
    });

    var series = $scope.series = [];

    var Series = function (vals, name, colour) {
        this.name = name;
        this.values = vals;
        this.colour = colour;
    };

    $scope.addSeries = function (name) {
        if (!name) {return;}

        var len = series.length + 1;
        series.push(new Series([name.a * len, name.b * len, name.c * len, name.d * len], name.name, name.colour));
        $scope.name = null;
    };

    $scope.delSeries = function (name) {
        if (!name) {return;}

        series.pop(name);
    };

    $scope.allClick = function (all) {
        if (all) {
            for (var i = 0; i < names.length; i++) {
                $scope.addSeries(names[i]);
            }
        } else {
            for (var i = 0; i < names.length + series.length; i++) {
                $scope.delSeries(names[i]);
            }
        }
    };

}]);