app.directive('lineChartDirective', ['lineChartConfig', function (lineChartConfig) {
    return {
        restrict: 'EA',
        replace: true,
        controller: 'LineChartController',
        template:
           '<svg ng-attr-height="{{height + 10}}" ng-attr-width="{{width + 10}}">' +
               '<g transform="translate(5, 5)">' +
                   '<line x1="0" y1="0" x2="0" ng-attr-y2="{{height}}" stroke="black" stroke-width="2"></line>' +
                   '<line x1="0" ng-attr-y1="{{height}}" ng-attr-x2="{{width}}" ng-attr-y2="{{height}}" stroke="black" stroke-width="2"></line>' +
                   '<g ng-attr-transform="translate(0, {{height}}), scale(1, -1)" ng-transclude>' +
                   '</g>' +
               '</g>' +
           '</svg>',
        transclude: true,
        scope: {
           h: '@h',
           w: '@w'
        },
        link: function (scope, element, attrs, ctrl) {
            scope.height = angular.isDefined(scope.h) ? scope.$eval(scope.h) : lineChartConfig.height;
            scope.width = angular.isDefined(scope.w) ? scope.$eval(scope.w): lineChartConfig.width;
        }
    };
}]);