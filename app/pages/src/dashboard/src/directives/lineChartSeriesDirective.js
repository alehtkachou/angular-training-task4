app.directive('lineChartSeriesDirective', ['$parse', function ($parse) {
    return {
        restrict: 'EA',
        require: '^lineChartDirective',
        replace: true,
        templateNamespace: 'svg',
        template:
            '<polyline ng-attr-points="{{points.join(\' \')}}" />',
        scope: {
            vals: '@values'
        },
        link: function (scope, element, attrs, ctrl) {
            var getValues = $parse(scope.vals);

            scope.values = getValues();
            ctrl.addSeries(scope);

            scope.$watchCollection('vals', function () {
                scope.values = $parse(scope.vals)();
                ctrl.setLines();
            });
        }
    };
}]);