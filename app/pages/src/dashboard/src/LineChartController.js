app.controller("LineChartController", ["$scope", "pointService", function ($scope, pointService) {
    var series;

    this.series = series = [];

    this.addSeries = function (seriesScope) {
        series.push(seriesScope);
    };

    this.setLines = function () {
        var maxValue = 0;
        var maxValuesLen = 0;

        angular.forEach(series, function (series) {
            if (series.values.length > maxValuesLen) maxValuesLen = series.values.length;
            angular.forEach(series.values, function (value) {
                if (value > maxValue) maxValue = value;
            });
        });

        angular.forEach(series, function (series) {
            series.points = [];
            angular.forEach(series.values, function (value, index) {
                var x = index * ($scope.width / (maxValuesLen -1));
                var y = value * ($scope.height / maxValue);
            series.points.push(new pointService(x, y));
            });
        });
    };
}]);