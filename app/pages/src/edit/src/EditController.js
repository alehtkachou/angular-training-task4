app.controller("EditController", ["$scope", "$state", "loginService", "auth", "$window", function ($scope, $state, loginService, auth, $window) {

    "use strict";

    $scope.name = JSON.parse($window.sessionStorage.getItem("userInfo")).userName;
    $scope.bio = JSON.parse($window.sessionStorage.getItem("userInfo")).userBio;

    $scope.submit = function (name, bio) {

        $scope.submitted = true;

        if ($scope.form.$invalid) {
            return;
        }

        var data = JSON.parse($window.sessionStorage.getItem("userInfo"));

        loginService.setStorage(data.accessToken, name, bio);

        $state.go("main.info");
    };

    $scope.submitted = false;

}]);