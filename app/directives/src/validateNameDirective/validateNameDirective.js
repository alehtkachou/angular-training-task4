app.directive('validateName', function () {

    "use strict";

    var REGEX_STRING = /^([A-Z][a-z]{3,})+\s+([A-Z][a-z]{3,})?$/;

    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {

            ctrl.$validators.name = function (viewValue) {

                if (REGEX_STRING.test(viewValue)) {
                    return true;
                }
                return false;
            };
        }
    };
});