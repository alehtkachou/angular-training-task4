app.directive('validateString', function () {

    "use strict";

    var REGEX_STRING = /^[a-zA-Z\s]*$/;

    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {

            ctrl.$validators.string = function (viewValue) {

                if (REGEX_STRING.test(viewValue)) {
                    return true;
                }
                return false;
            };
        }
    };
});