app.service("loginService", ["$http", "$q", "$window", function ($http, $q, $window) {

    "use strict";

    var userInfo;

    this.login = function (userName, password) {
        var deferred = $q.defer();

        $http.post("/login", { userName: userName, password: password })
            .then(function (result) {
                userInfo = {
                    accessToken: result.data.access_token,
                    userName: result.data.userName,
                    userBio: result.data.userBio
                };
                $window.sessionStorage["userInfo"] = JSON.stringify(userInfo);

                deferred.resolve(userInfo);
            }, function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    };

    this.logout = function () {
        var deferred = $q.defer();

        $http({
            method: "POST",
            url: "/logout",
            headers: {
                "access_token": userInfo.accessToken
            }
        }).then(function (result) {
            userInfo = null;

            $window.sessionStorage["userInfo"] = null;

            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    this.getUserInfo = function () {
        return userInfo;
    };

    this.setStorage = function (accessToken, newName, newBio) {
        $window.sessionStorage["userInfo"] = JSON.stringify({"accessToken": accessToken, "userName": newName, "userBio": newBio});
        userInfo = JSON.parse($window.sessionStorage["userInfo"]);
    };

    var init = function () {
        if ($window.sessionStorage["userInfo"]) {
            userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
    };
    init();

}]);