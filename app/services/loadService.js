app.service('loadService', ["$http", "$q", function ($http, $q) {

    "use strict";

    this.getUsers = function () {
        var deferred = $q.defer();

        $http.get('data/users.json')
            .then(function (data) {
                deferred.resolve(data);
            }, function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    };

    this.getDataChart = function () {
        var deferred = $q.defer();

        $http.get('data/dataChart.json')
            .then(function (data) {
                deferred.resolve(data);
            }, function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    };

}]);